require 'twitter'

task :test_task => :environment do

	client = Twitter::REST::Client.new do |config|
	  config.consumer_key        = ENV['consumer_key']
	  config.consumer_secret     = ENV['consumer_secret']
	  config.access_token        = ENV['access_token']
	  config.access_token_secret = ENV['access_token_secret']
	end

  past_retweets = Array.new
  client.retweeted_by_me(:count => 30).collect do |tweet|
    past_retweets.push(tweet.retweeted_status.id)
  end

  p "**new**"
  new_retweets = Array.new
  client.search("カレー作りすぎた", :result_type => "recent").take(20).collect do |tweet|  
    if tweet.user.screen_name == "too_much_curry"
      next
    end

    # リツイートを除く
    if tweet.text.start_with?("RT")
      next
    end
    # botを除く
    if tweet.user.screen_name.include?("bot") || tweet.user.name.include?("bot")
      next
    end
    # 過去にリツイートしたものを除く
    if past_retweets.include?(tweet.id)
      next
    end

    p "#{tweet.user.screen_name}: #{tweet.text}"
    new_retweets.push(tweet)
  end
    begin
      # 2件だけリツイート
      client.retweet(new_retweets[0,2])
    rescue Exception => e
      p "Error:"+e.message
    ensure
      # フォローする
      new_retweets[0,2].each do |tweet|
        begin
          client.follow(tweet.user.screen_name)
        rescue Exception => e
          p "Error:"+e.message
        end
      end
    end
end

task :daily_task => :environment do

	client = Twitter::REST::Client.new do |config|
	  config.consumer_key        = ENV['consumer_key']
	  config.consumer_secret     = ENV['consumer_secret']
	  config.access_token        = ENV['access_token']
	  config.access_token_secret = ENV['access_token_secret']
	end

	client.update("世の中のカレー作りすぎたを集めてリツイートしていきます。フォローするとおすそ分けをもらえるかもしれません。#カレー")

end